using Microsoft.EntityFrameworkCore;
using WebAPIWithEntityFramework.Models;
using WebAPIWithEntityFramework.Controllers;
using WebAPIWithEntityFramework.Services.Students;

namespace WebAPIWithEntityFramework
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            builder.Services.AddDbContext<PostgradDbContext>(
                opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("default")));
            builder.Services.AddScoped<IStudentService, StudentService>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}