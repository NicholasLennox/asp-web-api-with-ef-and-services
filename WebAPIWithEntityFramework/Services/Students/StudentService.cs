﻿using Microsoft.EntityFrameworkCore;
using WebAPIWithEntityFramework.Models;

namespace WebAPIWithEntityFramework.Services.Students
{
    public class StudentService : IStudentService
    {
        private readonly PostgradDbContext _context;

        public StudentService(PostgradDbContext context)
        {
            _context = context;
        }

        public async Task<Student> AddAsync(Student obj)
        {
            await _context.Students.AddAsync(obj);
            await _context.SaveChangesAsync();
            return obj;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Student>> GetAllAsync()
        {
            return await _context.Students.ToListAsync();
        }

        public async Task<Student> GetByIdAsync(int id)
        {
            return await _context.Students.FindAsync(id);
        }

        public async Task<IEnumerable<Subject>> GetSubjects(int id)
        {
            return (await _context.Students
                .Where(s => s.Id == id)
                .Include(s => s.Subjects)
                .FirstAsync()).Subjects;
        }

        public async Task<Student> UpdateAsync(Student obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return obj;
        }
    }
}
