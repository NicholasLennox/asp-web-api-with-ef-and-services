﻿using WebAPIWithEntityFramework.Models;

namespace WebAPIWithEntityFramework.Services.Students
{
    public interface IStudentService : ICrudService<Student, int>
    {
        Task<IEnumerable<Subject>> GetSubjects(int id);
    }
}
